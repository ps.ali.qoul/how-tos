package com.progressoft.workflow.bpmn.contract;

import lombok.Getter;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Getter
public class WorkflowListeners {

    public static final WorkflowListeners EMPTY = new WorkflowListeners(null, Collections.emptyMap());

    private final String workflowName;
    private final Map<String, List<WorkflowListener>> listeners;

    public WorkflowListeners(String workflowName, Map<String, List<WorkflowListener>> listeners) {
        this.workflowName = workflowName;
        this.listeners = listeners;
    }

    public List<WorkflowListener> getActionListener(String action) {
        return listeners.getOrDefault(action, Collections.emptyList());
    }
}
