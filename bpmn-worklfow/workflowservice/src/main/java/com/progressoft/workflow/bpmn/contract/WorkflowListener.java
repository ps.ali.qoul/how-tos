package com.progressoft.workflow.bpmn.contract;

public interface WorkflowListener {

    void execute(Object item);
}
