package com.progressoft.workflow.bpmn.contract;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class WorkflowAction {

    private final String name;

    public WorkflowAction(String name) {
        this.name = name;
    }
}
