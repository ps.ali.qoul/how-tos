package com.progressoft.workflow.bpmn;

import com.progressoft.workflow.bpmn.contract.ModelRepository;
import org.camunda.bpm.model.bpmn.Bpmn;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class BpmnModelRepository implements ModelRepository<BpmnModel> {

    private static final Map<String, BpmnModel> models = new HashMap<>();

    @Override
    public BpmnModel load(String name) {
        return models.computeIfAbsent(name, this::loadWorkflow);
    }

    private BpmnModel loadWorkflow(String name) {
        String resource = "/bpmn/" + name + ".bpmn";
        try (InputStream stream = this.getClass().getResourceAsStream(resource)) {
            return createModel(name, stream);
        } catch (Exception e) {
            throw new ModelLoadingError(e);
        }
    }

    private BpmnModel createModel(String name, InputStream stream) {
        return new BpmnModel(name, Bpmn.readModelFromStream(stream));
    }

    public static class ModelLoadingError extends RuntimeException {
        public ModelLoadingError(Exception e) {
            super(e);
        }
    }
}
