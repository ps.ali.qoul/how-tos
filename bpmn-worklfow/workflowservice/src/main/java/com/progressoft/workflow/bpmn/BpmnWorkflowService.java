package com.progressoft.workflow.bpmn;

import com.progressoft.workflow.bpmn.contract.*;
import de.odysseus.el.ExpressionFactoryImpl;
import de.odysseus.el.util.SimpleContext;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.instance.camunda.CamundaPropertiesImpl;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperty;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import java.util.*;
import java.util.stream.Collectors;

public class BpmnWorkflowService implements WorkflowService {

    private final ModelRepository<BpmnModel> repository;
    private final Map<String, WorkflowListeners> listeners;

    public BpmnWorkflowService(ModelRepository<BpmnModel> repository, Map<String, WorkflowListeners> listeners) {
        this.repository = repository;
        this.listeners = listeners;
    }

    @Override
    public WorkflowStep start(String workflowName) {
        BpmnModelInstance model = repository.load(workflowName).get();
        //TODO: all workflows should have their start elements named as "start"
        return model.<StartEvent>getModelElementById("start").getOutgoing().stream()
                .findFirst()
                .map(seq -> model.<SequenceFlow>getModelElementById(seq.getId()))
                .map(SequenceFlow::getTarget)
                .map(t -> new WorkflowStep(workflowName, t.getId(), t.getName(), t instanceof EndEvent,
                        mapActions(t), mapVariables(t)))
                .orElse(WorkflowStep.EMPTY);
    }

    @Override
    public WorkflowStep getNextStep(WorkflowState state) {
        BpmnModelInstance model = repository.load(state.getWorkflow()).get();
        FlowNode currentElement = model.getModelElementById(state.getCurrentStepId());
        FlowNode next = findNextTaskStep(model, currentElement, state.getAction());

        WorkflowStep result = Optional.ofNullable(next)
                .map(t -> new WorkflowStep(state.getWorkflow(), t.getId(), t.getName(), t instanceof EndEvent,
                        mapActions(t), mapVariables(t)))
                .orElse(WorkflowStep.EMPTY);
        listeners.getOrDefault(state.getWorkflow(), WorkflowListeners.EMPTY)
                .getActionListener(state.getAction())
                .forEach(l -> l.execute(state.getItem()));
        return result;
    }

    private FlowNode findNextTaskStep(BpmnModelInstance model, FlowNode node, String action) {
        if (node instanceof ExclusiveGateway)
            return nextNode(model, node, action);
        return node.getOutgoing().stream()
                .filter(a -> a.getTarget() instanceof ExclusiveGateway)
                .map(SequenceFlow::getTarget)
                .map(n -> nextNode(model, n, action))
                .findFirst()
                .orElse(node);
    }

    private FlowNode nextNode(BpmnModelInstance model, FlowNode currentElement, String action) {
        return currentElement.getOutgoing().stream()
                .filter(a -> evaluateExpression(a.getConditionExpression(), action))
                .findFirst()
                .map(seq -> model.<SequenceFlow>getModelElementById(seq.getId()))
                .map(SequenceFlow::getTarget)
                .orElse(null);
    }

    private boolean evaluateExpression(ConditionExpression expression, String action) {
        ExpressionFactory factory = new ExpressionFactoryImpl();
        SimpleContext context = new SimpleContext();
        context.setVariable("action", factory.createValueExpression(action, String.class));
        ValueExpression expr = factory.createValueExpression(context, expression.getTextContent(), Boolean.class);
        return Boolean.parseBoolean(expr.getValue(context).toString());
    }

    // All actions must have names

    @Override
    public List<WorkflowAction> getActions(WorkflowState state) {
        BpmnModelInstance model = repository.load(state.getWorkflow()).get();
        FlowNode currentElement = model.getModelElementById(state.getCurrentStepId());
        return mapActions(currentElement);
    }

    private List<WorkflowAction> mapActions(FlowNode node) {
        return node.getOutgoing().stream()
                .map(FlowElement::getName)
                .map(WorkflowAction::new)
                .collect(Collectors.toList());
    }

    private Map<String, Object> mapVariables(FlowNode node) {
        if(node.getExtensionElements() == null)
            return new HashMap<>();
        return node.getExtensionElements().getElements().stream()
                .map(e -> (CamundaPropertiesImpl) e)
                .map(CamundaPropertiesImpl::getCamundaProperties)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(CamundaProperty::getCamundaName, CamundaProperty::getCamundaValue));
    }
}
