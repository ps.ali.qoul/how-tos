package com.progressoft.workflow.bpmn.contract;

import java.util.List;

public interface WorkflowService {

    WorkflowStep start(String workflowName);
    WorkflowStep getNextStep(WorkflowState state);
    List<WorkflowAction> getActions(WorkflowState state);
}
