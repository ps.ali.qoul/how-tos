package com.progressoft.workflow.bpmn;

import com.progressoft.workflow.bpmn.contract.BpmnWorkflowModel;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;

public class BpmnModel implements BpmnWorkflowModel<BpmnModelInstance> {

    private final String name;
    private final BpmnModelInstance model;

    public BpmnModel(String name, BpmnModelInstance model) {
        this.name = name;
        this.model = model;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BpmnModelInstance get() {
        return model;
    }
}
