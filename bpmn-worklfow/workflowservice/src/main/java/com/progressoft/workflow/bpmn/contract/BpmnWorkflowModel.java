package com.progressoft.workflow.bpmn.contract;

public interface BpmnWorkflowModel<T> {

    String getName();

    T get();

}
