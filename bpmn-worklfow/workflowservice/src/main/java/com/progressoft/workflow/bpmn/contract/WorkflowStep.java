package com.progressoft.workflow.bpmn.contract;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class WorkflowStep {

    public static final WorkflowStep EMPTY = new WorkflowStep(null, null,null, false);

    private final String workflowName;
    private final String id;
    private final String name;
    private final boolean isEnd;
    private final List<WorkflowAction> actions = new ArrayList<>();
    private final Map<String, Object> variables = new HashMap<>();

    public WorkflowStep(String workflowName, String id, String name, boolean isEnd) {
        this(workflowName, id, name, isEnd, new ArrayList<>(), new HashMap<>());
    }

    public WorkflowStep(String workflowName, String id, String name, boolean isEnd,
                        List<WorkflowAction> actions, Map<String, Object> variables) {
        this.workflowName = workflowName;
        this.id = id;
        this.name = name;
        this.isEnd = isEnd;
        this.actions.addAll(actions);
        this.variables.putAll(variables);
    }
}
