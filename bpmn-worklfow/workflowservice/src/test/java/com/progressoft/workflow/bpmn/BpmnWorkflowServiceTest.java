package com.progressoft.workflow.bpmn;

import com.progressoft.workflow.bpmn.contract.ModelRepository;
import com.progressoft.workflow.bpmn.contract.WorkflowAction;
import com.progressoft.workflow.bpmn.contract.WorkflowState;
import com.progressoft.workflow.bpmn.contract.WorkflowStep;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class BpmnWorkflowServiceTest {

    private static final String WORKFLOW_NAME = "MAKER_CHECKER";

    private final ModelRepository<BpmnModel> repository = new BpmnModelRepository();
    private final BpmnWorkflowService service = new BpmnWorkflowService(repository, Collections.emptyMap());

    @Test
    void whenStartWorkflowThenReturnNextStep() {
        WorkflowStep step = service.start(WORKFLOW_NAME);

        assertEquals("Verifier", step.getName());
        assertFalse(step.isEnd());
        assertEquals(WORKFLOW_NAME, step.getWorkflowName());
        assertTrue(step.getActions().contains(new WorkflowAction(null)));
    }

    @Test
    void givenNextStepIsExclusiveGatewayWhenActionIsApproveThenReturnNextWorkflowStepAfterEvaluatingExpression() {
        WorkflowStep step = service.getNextStep(new WorkflowState(WORKFLOW_NAME, "Task_0v1hwll", "verifierApprove", null));

        assertTrue(step.isEnd());
        assertEquals(WORKFLOW_NAME, step.getWorkflowName());
        assertTrue(step.getActions().isEmpty());
    }

    @Test
    void givenNextStepIsExclusiveGatewayWhenActionIsRepairThenReturnNextWorkflowStepAfterEvaluatingExpression() {
        WorkflowStep step = service.getNextStep(new WorkflowState(WORKFLOW_NAME, "Task_0v1hwll", "verifierRepair", null));

        assertFalse(step.isEnd());
        assertEquals(WORKFLOW_NAME, step.getWorkflowName());
        assertEquals("Data Entry", step.getName());
        assertTrue(step.getActions().contains(new WorkflowAction("Verify")));
        assertEquals(1, step.getVariables().size());
        assertEquals("true", step.getVariables().get("editable"));
    }
}