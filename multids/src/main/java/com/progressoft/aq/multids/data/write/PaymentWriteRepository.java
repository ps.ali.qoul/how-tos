package com.progressoft.aq.multids.data.write;

import com.progressoft.aq.multids.data.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentWriteRepository extends JpaRepository<Payment, Long> {
}
