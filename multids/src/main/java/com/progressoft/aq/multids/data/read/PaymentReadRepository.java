package com.progressoft.aq.multids.data.read;

import com.progressoft.aq.multids.data.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentReadRepository extends CrudRepository<Payment, Long> {

    @Override
    List<Payment> findAll();
}
