package com.progressoft.aq.multids;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultidsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultidsApplication.class, args);
    }

}
