package com.progressoft.aq.multids.web;

import com.progressoft.aq.multids.data.Payment;
import com.progressoft.aq.multids.data.read.PaymentReadRepository;
import com.progressoft.aq.multids.data.write.PaymentWriteRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/payments")
public class PaymentResource {

    private final PaymentWriteRepository writeRepository;
    private final PaymentReadRepository readRepository;

    public PaymentResource(PaymentWriteRepository writeRepository, PaymentReadRepository readRepository) {
        this.writeRepository = writeRepository;
        this.readRepository = readRepository;
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional("writeTransactionManager")
    public Long create(@RequestBody Payment payment) {
        return writeRepository.save(payment).getId();
    }

    @GetMapping("/")
    public ResponseEntity<List<Payment>> get() {
        return ResponseEntity.ok(readRepository.findAll());
    }

    @GetMapping("/writedb")
    @ResponseStatus(HttpStatus.OK)
    @Transactional("writeTransactionManager")
    public ResponseEntity<List<Payment>> getFromWriteDb() {
        return ResponseEntity.ok(writeRepository.findAll());
    }
}
