package com.progressoft.aq.structured;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.StructuredDataMessage;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

@RequestMapping("/persons")
@RestController
public class PersonController {

    private final Logger logger = LogManager.getLogger(PersonController.class);

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Person person) {
        Map<String, String> logData = Map.of("SpanId", "ID as extra value", "PersonName", person.getName());
        StructuredDataMessage msg = new StructuredDataMessage("Id from SDM", "log message", "a type", logData);
        logger.info(msg);
    }

}