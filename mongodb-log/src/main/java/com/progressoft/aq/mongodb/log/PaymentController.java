package com.progressoft.aq.mongodb.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.MDC;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RequestMapping("/payments")
@RestController
public class PaymentController {

    private final Logger logger = LogManager.getLogger(PaymentController.class);

    @PostMapping("/")
    public ResponseEntity<?> create(@RequestBody Payment payment, UriComponentsBuilder builder) {
        logMessage(payment);

        Payment result = save(payment);
        UriComponents uriComponents =
                builder.path("/payments/{id}").buildAndExpand(result.getId());

        logger.info("finished");
        MDC.remove("messageId");
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    private void logMessage(Payment payment) {
        MDC.put("isMsg", "true");
        MDC.put("messageId", payment.getMessageId());
        logger.info("*******************************************");
        logger.info("received request ot create a payment with message : '{}'", payment);
        logger.info("*******************************************");
        MDC.remove("isMsg");
    }

    private Payment save(Payment payment) {
        MDC.put("messageId", payment.getMessageId());
        logger.info("saving payment");
        // do saving logic, here we are simulating it
        payment.setId(System.currentTimeMillis());
        return payment;
    }
}