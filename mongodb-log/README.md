# Logging

This project introduces the following logging techniques:

## Ship Application Logs to MongoDB
In this technique, the application appends it logs to a MongoDB collection.

[MongoDB](src/main/java/com/progressoft/aq/mongodb/log/PaymentController.java)

## Structured Logging
In this technique, the application added extra logging information as custom fields and then those logs are represented as JSON format

[Structured](src/main/java/com/progressoft/aq/structured/PersonController.java)

